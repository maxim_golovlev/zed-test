//
//  CircularCollectionView.swift
//  GisGes
//
//  Created by Admin on 30.11.16.
//  Copyright © 2016 vice3.agency. All rights reserved.
//

import UIKit
import RxSwift
import RxSwiftExt

class CircularCollectionView: UICollectionView {

    let items = Variable<[Genre]>([])
    var selectedItemChanged: ((Genre) -> ())?
    var centerOffset: CGFloat?
    var contentOffsetWhenFullyScrolledRight: CGFloat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        delegate = self
        collectionViewLayout = CenterLayout()
    }
}

extension CircularCollectionView: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        contentOffsetWhenFullyScrolledRight = scrollView.contentSize.width - scrollView.bounds.width - (centerOffset ?? 0)
        if scrollView.contentOffset.x >= contentOffsetWhenFullyScrolledRight! {
            let newIndexPath = IndexPath(item: items.value.count - 4, section: 0)
            self.scrollToItem(at: newIndexPath, at: .right, animated: false)
            updateOffset(increased: false)
            
        } else if scrollView.contentOffset.x == (centerOffset ?? 0) {
            let newIndexPath = IndexPath(item: 3, section: 0)
            self.scrollToItem(at: newIndexPath, at: .left, animated: false)
            updateOffset(increased: true)
        }

        self.updateSelectedItem()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        visibleCells.forEach { (cell) in
            cell.layer.borderWidth = 0
        }
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        updateSelectedItem()
    }
    
    func updateSelectedItem() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [unowned self] in
            self.getCenterCell ( center: { cell in
                guard let selectedIndex = self.indexPath(for: cell)?.row else { return }
                if let selectedItemChanged = self.selectedItemChanged { selectedItemChanged(self.items.value[selectedIndex]) }
                cell.layer.borderWidth = 1
                cell.layer.borderColor = UIColor.hexStringToUIColor(hex: "#f7a700").cgColor
            })
        }
    }
    
    func getCenterCell( center: (UICollectionViewCell) -> ()) {
        
        visibleCells.forEach { cell in
            let cellFrameInSuperview = self.convert(cell.frame, to: self.superview)
            if cellFrameInSuperview.origin.x < self.bounds.width/2
                && (cellFrameInSuperview.origin.x + cell.bounds.width) > self.bounds.width/2 {
                center(cell)
            }
        }
    }
    
    func updateOffset(increased: Bool) {
        var newOffset = self.contentOffset
        newOffset.x += increased ? (centerOffset ?? 0) : -(centerOffset ?? 0)
        setContentOffset(newOffset , animated: false)
    }
}

extension CircularCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 70)
    }
}

class CenterLayout : UICollectionViewFlowLayout {
    
    let activeDistance = CGFloat(100)
    let zoomFactor = CGFloat(0.5)
    var centerOffset: ((CGFloat) -> ())?
    
    override func prepare() {
        super.prepare()
        
        itemSize = CGSize(width: 100, height: 70)
        scrollDirection = .horizontal
        minimumLineSpacing = 30
        sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        super.shouldInvalidateLayout(forBoundsChange: newBounds)
        return true
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        guard let cv = collectionView else { return super.layoutAttributesForElements(in: rect) }
        
        let array = super.layoutAttributesForElements(in: rect)
        let visibleRect = CGRect(origin: cv.contentOffset, size: cv.bounds.size)

        array?.forEach({ attributes in
            
            if attributes.frame.intersects(rect) {
                let distance = visibleRect.midX - attributes.center.x
                let normalizeDistance = distance / activeDistance
                
                if abs(distance) < activeDistance {
                    let zoom = CGFloat(1) + zoomFactor * (CGFloat(1) - abs(normalizeDistance) )
                    attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0)
                    attributes.zIndex = lroundf(Float(zoom))
                }
            }            
        })
        
        return array
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        if let cv = self.collectionView as? CircularCollectionView{
            
            let cvBounds = cv.bounds
            let halfWidth = cvBounds.size.width * 0.5;
            let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth;
            
            if let attributesForVisibleCells = self.layoutAttributesForElements(in: cvBounds) {
                
                var candidateAttributes : UICollectionViewLayoutAttributes?
                for attributes in attributesForVisibleCells {
                    
                    // == Skip comparison with non-cell items (headers and footers) == //
                    if attributes.representedElementCategory != UICollectionElementCategory.cell {
                        continue
                    }
                    
                    if let candAttrs = candidateAttributes {
                        
                        let a = attributes.center.x - proposedContentOffsetCenterX
                        let b = candAttrs.center.x - proposedContentOffsetCenterX
                        
                        if fabsf(Float(a)) < fabsf(Float(b)) {
                            candidateAttributes = attributes;
                        }
                    }
                    else { // == First time in the loop == //
                        
                        candidateAttributes = attributes;
                        continue;
                    }
                }
                
                let newPoint = CGPoint(x : candidateAttributes!.center.x - halfWidth, y : proposedContentOffset.y)

                if abs((cv.contentOffsetWhenFullyScrolledRight ?? 0) - newPoint.x) < 100 {
                    cv.centerOffset = proposedContentOffset.x - newPoint.x
                } else if newPoint.x < 100 {
                    cv.centerOffset = newPoint.x - 0
                }
                
                return newPoint
            }
        }
        // Fallback
        return super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
    }
}

extension UIColor {
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
