//
//  MovieCell.swift
//  ZedTest
//
//  Created by Admin on 30.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleTemplateView: UIView! {
        didSet {
            titleTemplateView.layer.cornerRadius = titleTemplateView.bounds.height / 2
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    
    var movie: Movie? {
        didSet {
            self.imageView.image = movie?.image?.toSize(self.imageView.bounds.size)
            self.titleLabel.text = movie?.name
        }
    }
}
