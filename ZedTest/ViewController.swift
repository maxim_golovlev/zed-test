//
//  ViewController.swift
//  ZedTest
//
//  Created by Admin on 30.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {

    @IBOutlet weak var genreCollectionView: CircularCollectionView!
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    let disposeBag = DisposeBag()
    var selectedMovies = PublishSubject<[Movie]>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let action  = [Movie(image: #imageLiteral(resourceName: "assasin_creed"), name: "Assasin Creed"),
                       Movie(image: #imageLiteral(resourceName: "commando"), name: "Commando"),
                       Movie(image: #imageLiteral(resourceName: "gamer"), name :"Gamer")]
        
        let drama   = [Movie(image: #imageLiteral(resourceName: "Anesthesia"), name: "Anestethia"),
                       Movie(image: #imageLiteral(resourceName: "Intouchables"), name: "Intouchebles"),
                       Movie(image: #imageLiteral(resourceName: "mcFarland"), name: "McFarland")]
        
        let horror  = [Movie(image: #imageLiteral(resourceName: "forest"), name: "The Forest"),
                       Movie(image: #imageLiteral(resourceName: "the_ring"), name: "The Ring"),
                       Movie(image: #imageLiteral(resourceName: "The_Shining"), name: "The SHining")]
        
        let genres = Observable.of([Genre(image: #imageLiteral(resourceName: "horror"), name: "Horror", movies: horror),
                                    Genre(image: #imageLiteral(resourceName: "drama"), name: "Drama", movies: drama),
                                    Genre(image: #imageLiteral(resourceName: "action"), name: "Action", movies: action)].preparedToEndless)
        
        addBindings([
            
            GenreCollectionBinder.with(collectionView: genreCollectionView, items: genres, in: self, selected: { [unowned self] genre in
                self.selectedMovies.onNext(genre.movies)
                
                guard let selectedPath = self.genreCollectionView.indexPathsForSelectedItems?.first else {return}
                
                self.genreCollectionView.scrollToItem(at: selectedPath, at: .centeredHorizontally, animated: true)
            }),
            genres.bindTo(genreCollectionView.items),
            
            MovieCollectionBinder.with(collectionView: moviesCollectionView, items: selectedMovies, in: self)
        ])
        
        genreCollectionView.selectedItemChanged = { [unowned self] genre in
            self.selectedMovies.onNext(genre.movies)
        }
        
        self.selectedMovies.onNext(horror)        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        genreCollectionView.getCenterCell(center: { [unowned self] (cell) in
            guard let selectedIndex = self.genreCollectionView.indexPath(for: cell) else { return }
            self.genreCollectionView.scrollToItem(at: selectedIndex, at: .centeredHorizontally, animated: false)
            self.genreCollectionView.updateSelectedItem()
        })
    }

    func dispose(_ disposable: Disposable) {
        disposable.addDisposableTo(disposeBag)
    }
    
    func addBindings(_ disposables: [Disposable]) {
        disposables.forEach { d in dispose(d) }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        genreCollectionView.collectionViewLayout.invalidateLayout()
        moviesCollectionView.collectionViewLayout.invalidateLayout()
    }
}

class Genre {
    var image: UIImage?
    var name: String?
    var movies = [Movie]([])
    
    init(image: UIImage, name: String, movies: [Movie]) {
        self.image = image
        self.name = name
        self.movies = movies
    }
}

class Movie {
    var image: UIImage?
    var name: String?
    
    init(image: UIImage?, name: String?) {
        self.image = image
        self.name = name
    }
}

extension Array {
    var preparedToEndless: Array {
        let firts = self
        let last = self
        var items = self
        if count >= 1 {
            items.insert(contentsOf: firts, at: 0)
            items.insert(contentsOf: firts, at: 0)
            items.append(contentsOf: last)
            items.append(contentsOf: last)
        }
        return items
    }
}

