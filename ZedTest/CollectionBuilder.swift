//
//  CollectionBuilder.swift
//  ZedTest
//
//  Created by Admin on 30.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

protocol CollectionBinder {
}

class GenreCollectionBinder: CollectionBinder {
    static func with(collectionView: UICollectionView, items: Observable<[Genre]>, `in`: ViewController, selected: ((_ element: Genre) -> Void)? = nil) -> Disposable {
        return CompositeDisposable(
            items.bindTo(collectionView.rx.items(cellIdentifier: "GenreCell", cellType: GenreCell.self)) { (row, element, cell) in
                cell.genre = element
            },
            collectionView.rx.modelSelected(Genre.self).subscribe(onNext: { element in
                if let selected = selected { selected(element) }
            })
        )
    }
}

class MovieCollectionBinder: CollectionBinder {
    static func with(collectionView: UICollectionView, items: Observable<[Movie]>, `in`: ViewController) -> Disposable {
        return CompositeDisposable(
            items.bindTo(collectionView.rx.items(cellIdentifier: "MovieCell", cellType: MovieCell.self)) { (row, element, cell) in
                cell.movie = element
            },
            collectionView.rx.modelSelected(Movie.self).subscribe(onNext: { element in
            })
        )
    }
}
