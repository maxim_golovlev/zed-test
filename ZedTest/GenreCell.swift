//
//  GenreCell.swift
//  ZedTest
//
//  Created by Admin on 30.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class GenreCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var genre: Genre? {
        didSet {
            self.imageView.image = genre?.image?.toSize(self.imageView.bounds.size)
            self.titleLabel.text = genre?.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 4
    }
}
