//
//  UIImageExt.swift
//  ZedTest
//
//  Created by Admin on 01.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

extension UIImage {
    
    func toSize(_ size: CGSize, aspectFit: Bool = false) -> UIImage {
        if aspectFit {
            let image = self
            var returnImage: UIImage?
            
            var scaleFactor: CGFloat = 1.0
            var scaledWidth = size.width
            var scaledHeight = size.height
            var thumbnailPoint = CGPoint(x: 0, y: 0)
            
            if !image.size.equalTo(size) {
                let widthFactor = size.width / image.size.width
                let heightFactor = size.height / image.size.height
                
                if widthFactor > heightFactor {
                    scaleFactor = widthFactor
                } else {
                    scaleFactor = heightFactor
                }
                
                scaledWidth = image.size.width * scaleFactor
                scaledHeight = image.size.height * scaleFactor
                
                if widthFactor > heightFactor {
                    thumbnailPoint.y = (size.height - scaledHeight) * 0.5
                } else if widthFactor < heightFactor {
                    thumbnailPoint.x = (size.width - scaledWidth) * 0.5
                }
            }
            
            UIGraphicsBeginImageContextWithOptions(size, true, 0)
            
            var thumbnailRect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize.zero)
            thumbnailRect.origin = thumbnailPoint
            thumbnailRect.size.width = scaledWidth
            thumbnailRect.size.height = scaledHeight
            
            image.draw(in: thumbnailRect)
            returnImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return returnImage!
        }
        else {
            UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
            draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image!.withRenderingMode(.alwaysOriginal)
        }
    }
}
